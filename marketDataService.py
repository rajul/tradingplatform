# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 10:12:21 2020

@author: hongsong chou
"""

import time
import os
import pandas as pd
from datetime import timedelta
import sys

from common import config
from common.OrderBookSnapshot_FiveLevels import OrderBookSnapshot_FiveLevels

# print(os.listdir('../'))
sys.path.append('../processedData/')


class MarketDataService:
    FUTURES_FILE = config.FUTURES_FILE
    STOCKS_FILE = config.STOCKS_FILE
    DATE = config.DATE

    def __init__(self, subscribers=[]):
        print("[%d]<<<<< call MarketDataService.init" % (os.getpid(),))
        self.data = self.preprocess()
        self.subscribers = subscribers

        time.sleep(3)
        self.produce_market_data()

    def preprocess(self):
        print('processing data......')

        # Reading files
        future_CDFD9 = pd.read_csv(self.FUTURES_FILE, index_col=0)
        stock2330 = pd.read_csv(self.STOCKS_FILE, index_col=0)

        # Dropping unneccesary columns and adding ticker column
        stock2330.drop(['lastPx', 'size', 'volume', 'filledFlag', 'stopMatch', 'testMatch'], axis=1, inplace=True)
        stock2330['ticker'] = 'stock2330'

        future_CDFD9.drop(['deriveBuyPrice', 'deriveSellPrice', 'deriveBuyQty', 'deriveSellQty', 'deriveFlag'], axis=1,
                          inplace=True)
        future_CDFD9['ticker'] = 'futureCDFD9'

        # Creating final data frame
        merged_data = pd.concat([stock2330, future_CDFD9], axis=0).sort_values(by='time')
        merged_data.reset_index(drop=True, inplace=True)

        merged_data = merged_data.loc[merged_data['time'] >= 90000000]
        merged_data.reset_index(drop=True, inplace=True)

        # Adding interval column in the dataframe to stream market data at the same speed as history
        merged_data['time'] = pd.to_datetime('20190401' + merged_data['time'].astype('str'), format='%Y%m%d%H%M%S%f')
        merged_data['interval'] = merged_data['time'].diff(1)
        merged_data['interval'] = merged_data['interval'].shift(-1)
        merged_data['interval'].fillna(timedelta(seconds=0), inplace=True)
        merged_data['time'] = merged_data['time'].dt.time

        return merged_data

    def produce_market_data(self):
        number_of_rows = self.data.shape[0]

        # Iterate over the data one row at a time
        for i in range(number_of_rows):
            data_row = self.data.loc[i]

            # produce a quote and push to subscribed services
            self.produce_quote(data_row)

            # Sleep until next quote needs to be produced
            time.sleep(data_row.interval.total_seconds()/1)

    def produce_quote(self, data_row):
        # print('[%d]MarketDataService>>>produce_quote' % (os.getpid()))
        bid_price = [data_row.bidPrice1, data_row.bidPrice2, data_row.bidPrice3, data_row.bidPrice4, data_row.bidPrice5]
        ask_price = [data_row.askPrice1, data_row.askPrice2, data_row.askPrice3, data_row.askPrice4, data_row.askPrice5]
        bid_size = [data_row.bidSize1, data_row.bidSize2, data_row.bidSize3, data_row.bidSize4, data_row.bidSize5]
        ask_size = [data_row.askSize1, data_row.askSize2, data_row.askSize3, data_row.askSize4, data_row.askSize5]

        quote_snapshot = OrderBookSnapshot_FiveLevels(data_row.ticker, self.DATE, data_row.time,
                                                     bid_price, ask_price, bid_size, ask_size)

        #print(quote_snapshot.ticker, quote_snapshot.timeStamp, quote_snapshot.bidPrice1, quote_snapshot.askPrice1)

        self.push_to_all_subscribers(quote_snapshot)

    def push_to_all_subscribers(self, quote_snapshot):
        for subscribers in self.subscribers:
            subscribers.put(quote_snapshot)
