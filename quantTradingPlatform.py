# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 10:15:48 2020

@author: hongsong chou
"""

import threading
import os
import time
from datetime import datetime
from common.SingleStockOrder import SingleStockOrder
from SingleStock_SingleStockFuturesArbitrageStrategy import SingleStock_SingleStockFuturesArbitrageStrategy
from SingleStock_SingleStockFuturesArbitrageStrategy import SingleStock_SingleStockFuturesArbitrageStrategy_lgbm
# from SingleStock_SingleStockFuturesArbitrageStrategy import SingleStock_SingleStockFuturesArbitrageStrategy_rsi
from multiprocessing import Process
from multiprocessing.pool import ThreadPool

# Support multiple strategies
# Synchronise data
#   - aggressive
#   - defensive
# Passing Orders and Execution
# Pair up order


class TradingPlatform:
    attached_strategies = {}
    # strategy_queues = {}

    ssfArbStrat = None

    def __init__(self, marketData_2_platform_q, platform_2_exchSim_order_q, exchSim_2_platform_execution_q):
        print("[%d]<<<<< call Platform.init" % (os.getpid(),))

        self.most_recent_stock_quote_time = None
        self.most_recent_future_quote_time = None
        self.most_recent_stock_quote = None
        self.most_recent_future_quote = None

        #Instantiate individual strategies
        ssfArbStrat = SingleStock_SingleStockFuturesArbitrageStrategy(
            "spread", "singleStock_singleStockFuturesArbStrategy", "testuser", "2330", "2020-08-13"
        )
        ssfArbStrat_lgbm = SingleStock_SingleStockFuturesArbitrageStrategy_lgbm(
            "lgbm+spread", "singleStock_singleStockFuturesArbStrategy_lgbm", "testuser", "2330", "2020-08-13"
        )

        # self.ssfArbStrat_rsi = SingleStock_SingleStockFuturesArbitrageStrategy_rsi(
        #     "rsi", "singleStock_singleStockFuturesArbStrategy_rsi", "testuser", "2330", "2020-08-13"
        # )

        self.attached_strategies = {
            'Strategy_1': ssfArbStrat,
            'Strategy_2': ssfArbStrat_lgbm,
            # 'Strategy_3': ssfArbStrat_rsi
        }

        self.strategy_wise_pending_orders = {
            'Strategy_1': set(),
            'Strategy_2': set()
        }

        t_md = threading.Thread(name='platform.on_marketData', target=self.consume_marketData, args=(platform_2_exchSim_order_q, marketData_2_platform_q,))
        t_md.start()

        t_exec = threading.Thread(name='platform.on_exec', target=self.handle_execution, args=(exchSim_2_platform_execution_q, ))
        t_exec.start()

    def consume_marketData(self, platform_2_exchSim_order_q, marketData_2_platform_q):
        #print('[%d]Platform.consume_marketData' % (os.getpid(),))
        while True:
            data = marketData_2_platform_q.get()

            #print('[%d] Platform.on_md' % (os.getpid()))
            #print(data.outputAsDataFrame())

            if data.ticker.startswith('future'):
                self.most_recent_future_quote_time = data.timeStamp
                self.most_recent_future_quote = data
            elif data.ticker.startswith('stock'):
                self.most_recent_stock_quote_time = data.timeStamp
                self.most_recent_stock_quote = data
            
            if not (self.most_recent_future_quote_time and self.most_recent_stock_quote_time):
                continue
            
            # p1 = Process(target = self.main_strategy, args=(platform_2_exchSim_order_q, ))
            # p1.start()
            # p2 = Process(target = self.sub_strategy)
            # p2.start()
            # self.ssfArbStrat.run(self.most_recent_stock_quote , self.most_recent_future_quote, None)
            # self.ssfArbStrat_rsi.run(self.most_recent_stock_quote , self.most_recent_future_quote, None)
            results = {}

            for (strategy_id, strategy) in self.strategies.items():
                results[strategy_id] = strategy.run(self.most_recent_stock_quote , self.most_recent_future_quote, None)

            # result = self.ssfArbStrat_lgbm.run(self.most_recent_stock_quote , self.most_recent_future_quote, None)
            for (strategy_id, result) in results.items():
                if result is None:
                    continue
                else:
                    print(result)
                #Create the pair of orders order
                date_today = datetime.today().strftime('%Y-%m-%d')
                order_1 = self.create_order(result[0], date_today, time.asctime(time.localtime(time.time())), result[2], result[1], result[3], "MO")
                order_2 = self.create_order(result[4], date_today, time.asctime(time.localtime(time.time())), result[6], result[5], result[7], "MO")

                self.strategy_wise_pending_orders[strategy_id].add(order_1.orderID)
                self.strategy_wise_pending_orders[strategy_id].add(order_2.orderID)

                pair_order = [order_1, order_2]

                #Submit the order to the exchange
                self.submit_pair_order_to_exchange(pair_order, platform_2_exchSim_order_q)

    def submit_pair_order_to_exchange(self, pair_order, platform_2_exchSim_order_q):
        for order in pair_order:
            platform_2_exchSim_order_q.put(order)

    def create_order(self, ticker, date, submission_time, price, direction, size, type1):
        order_1 = SingleStockOrder(ticker, date, submission_time)
        order_1.price = price
        order_1.direction = direction
        order_1.size = size
        order_1.type = type1
        return order_1

    def handle_execution(self, exchSim_2_platform_execution_q):
        #print('[%d]Platform.handle_execution' % (os.getpid(),))
        while True:
            #Get the execution from the exchange and send to the strategy
            execution = exchSim_2_platform_execution_q.get()
            #if execution.timeStamp != order_time and execution.ticker != ticker:
            print('[%d] Platform.handle_execution' % (os.getpid()))
            print(execution.outputAsArray())

            for (strategy_id, strategy) in self.strategies.items():
                if execution.orderID in self.strategy_wise_pending_orders[strategy_id]:
                    strategy.run(None, None, execution)
                    self.strategy_wise_pending_orders[strategy_id].remove(execution.orderID)
                    break
