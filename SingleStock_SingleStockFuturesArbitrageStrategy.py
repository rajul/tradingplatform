#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
Created on Thu Jun 20 10:26:05 2020

@author: hongsong chou
"""

import os
import time
from common.OrderBookSnapshot_FiveLevels import OrderBookSnapshot_FiveLevels
from common.Strategy import Strategy
from common.SingleStockOrder import SingleStockOrder
from common.SingleStockExecution import SingleStockExecution
import numpy as np
import joblib
import pandas as pd
import matplotlib.pyplot as plt


# import talib as ta
# import termplotlib as tpl

class SingleStock_SingleStockFuturesArbitrageStrategy(Strategy):
    
    def __init__(self, stratID, stratName, stratAuthor, ticker, day):
        super(SingleStock_SingleStockFuturesArbitrageStrategy, self).__init__(stratID, stratName, stratAuthor) #call constructor of parent
        self.ticker = ticker #public field
        self.day = day #public field
        #timestamp, direction, stock_ticker, stock_price, stock_position, futrues_ticker, futures_price, futures_position, cash
        self.stats_info = [None, None, 'stock2330', None, 0, 'futureCDFD9', None, 0, 0, 0, 'Spread'] 
        self.cash_list, self.timeStamp = [], []
        
    def getStratDay(self):
        return self.day
    
    
    def run(self, stockData, futuresData, execution):
        #print(stockData.outputAsDataFrame())#,isinstance(stockData, OrderBookSnapshot_FiveLevels))
        #print(futuresData.outputAsDataFrame())#, isinstance(futuresData, OrderBookSnapshot_FiveLevels))
        #print(execution.outputAsArray())
        # def update_info(info, last_trade):
        def update_info(info, trade, stock_midQ, futures_midQ):
            #self.stats_info[1] = last_trade
            self.stats_info[3] = info[2]
            self.stats_info[6] = info[6]
            # if trade == 'refill':
            #     self.stats_info[4] = 0
            #     self.stats_info[7] = 0
            flag = 0
            if trade == 'long':
                flag = -1
                self.stats_info[4] = self.stats_info[4] + 2
                self.stats_info[7] = self.stats_info[7] - 2
            elif trade == 'short':
                flag = 1
                self.stats_info[4] = self.stats_info[4] - 2
                self.stats_info[7] = self.stats_info[7] + 2
            self.stats_info[8] = self.stats_info[8] + flag*(2 * float(self.stats_info[3]) - 2 * float(self.stats_info[6]))
            self.stats_info[9] = self.stats_info[8] + (self.stats_info[4] * stock_midQ + self.stats_info[7] * futures_midQ)
            if self.stats_info[1] is None or self.stats_info[1] == 'refill':
                self.stats_info[1] = trade
            else:
                self.stats_info[1] = 'refill'

        def update_line(timeStamp, cash, direction):

            plt.close()
            if direction is not None:
                self.cash_list.append(cash)
                self.timeStamp.append(int(timeStamp.strftime("%H%M%S")))
                plt.plot(self.timeStamp, self.cash_list)
                plt.title('Ask & Bid Spread Arbitrage Strategy with 1 Std')
                plt.xlabel('Time')
                plt.ylabel('Total Asset')
                plt.show()
                plt.close()
                # fig.plot(self.timeStamp, self.cash_list)
                # fig.show()
            else:
                if len(self.timeStamp) >= 1 and int(timeStamp.strftime("%H%M%S")) - self.timeStamp[-1]>1000:
                        self.cash_list.append(cash)
                        self.timeStamp.append(int(timeStamp.strftime("%H%M%S")))
                        plt.plot(self.timeStamp, self.cash_list)
                        plt.title('Ask & Bid Spread Arbitrage Strategy with 1 Std')
                        plt.xlabel('Time')
                        plt.ylabel('Total Asset')
                        plt.show()
                        plt.close()
                        self.stats_info[0] = time.asctime(time.localtime(time.time()))
                        print(self.stats_info)
                        # fig.plot(self.timeStamp, self.cash_list)
                        # fig.show()
                       
        if (stockData is None) and (futuresData is None) and (execution is None):
            return None
        elif (stockData is None) and (futuresData is None) and ((execution is not None) and (isinstance(execution, SingleStockExecution))): # Try to refill last trade
            # #handle executions
            # if execution.ticker == 'stock2330':
            #     self.stats_info[3] = execution.price
            #print(execution.outputAsArray())
            if execution.ticker == 'futureCDFD9':
                #self.stats_info[3] = execution.price
                print('[%d] Strategy.handle_execution' % (os.getpid()))
                self.stats_info[0] = execution.timeStamp
                print('Current position and cash stats: >>>>>')
                print(self.stats_info)
        elif (((stockData is not None) and (futuresData is not None)) and (isinstance(stockData, OrderBookSnapshot_FiveLevels))) and (execution is None): # Try to create a new trade
            avg = -0.00014307391813982525
            std = 0.0016686150246379538
            futures_sp = futuresData.bidPrice1
            futures_bp = futuresData.askPrice1
            futures_midQ = (futures_sp+futures_bp)/2
            stock_sp, s_size = stockData.bidPrice1, stockData.bidSize1
            stock_bp, b_size = stockData.askPrice1, stockData.askSize1
            stock_midQ = (stock_sp+stock_bp)/2
            long = ('stock2330', 'buy', stock_bp, 2, 'futureCDFD9', 'sell', futures_sp, 1)
            short = ('stock2330', 'sell', stock_sp, 2, 'futureCDFD9', 'buy', futures_bp, 1)
            #self.stats_info[9] = self.stats_info[8] + (self.stats_info[4] * stock_midQ + self.stats_info[7] * futures_midQ)
            #handle new market data, then create a new order and send it via quantTradingPlatform
            #print(futures_sp, futures_bp, stock_sp, s_size, stock_bp, b_size)
            multiple = 1
            #print(self.stats_info)
            ### If there is no order placed before
            if self.stats_info[1] is None or self.stats_info[1] == 'refill':
                if s_size < 2 and b_size < 2: # cannot place order
                    update_line(stockData.timeStamp, self.stats_info[9], None)
                    return None
                elif s_size < 2 and b_size >= 2: # can only long spread
                    spread = np.log(stock_bp/futures_sp) #calculate long spread  
                    if spread > avg - multiple*std: # if not achieve the spread return None
                        update_line(stockData.timeStamp, self.stats_info[9], None)
                        return None
                    else:

                        update_info(long, 'long',stock_midQ, futures_midQ)
                        update_line(stockData.timeStamp, self.stats_info[9], 'long')
                        return long
                elif s_size >= 2 and b_size < 2: # can only short spread
                    spread = np.log(stock_sp/futures_bp)
                    if spread < avg + multiple*std: # if not achieve the spread return None
                        update_line(stockData.timeStamp, self.stats_info[9], None)
                        return None
                    else:
                        #short_cash = 2000*(stock_sp-futures_bp)
                        update_info(short, 'short', stock_midQ, futures_midQ)
                        update_line(stockData.timeStamp, self.stats_info[9], 'short')
                        return short
                else: # both do else short and long
                    spread_b = np.log(stock_bp/futures_sp)
                    spread_s = np.log(stock_sp/futures_bp)
                    diff_b = avg-multiple*std-spread_b
                    diff_s = spread_s-avg-multiple*std
                    if diff_b >= 0 and diff_b > diff_s:
                        update_info(long, 'long',stock_midQ, futures_midQ)
                        update_line(stockData.timeStamp, self.stats_info[9], 'long')
                        return long
                    elif diff_s >=0 and diff_s > diff_b:
                        update_info(short, 'short', stock_midQ, futures_midQ)
                        update_line(stockData.timeStamp, self.stats_info[9], 'short')
                        return short
                    elif diff_s >= 0 and diff_b >= 0:
                        if diff_s > diff_b:
                            update_info(short, 'short', stock_midQ, futures_midQ)
                            update_line(stockData.timeStamp, self.stats_info[9], 'short')
                            return short
                        else:
                            update_info(long, 'long', stock_midQ, futures_midQ)
                            update_line(stockData.timeStamp, self.stats_info[9], 'long')
                            return long
                    else:
                        update_line(stockData.timeStamp, self.stats_info[9], None)
                        return None
            else:
                if self.stats_info[1] == 'long':
                    if s_size >= 2:
                        if np.log(stock_sp/futures_bp)  >= avg:
                            update_info(short, 'short', stock_midQ, futures_midQ)
                            update_line(stockData.timeStamp, self.stats_info[9], 'short')
                            return short
                        else:
                            update_line(stockData.timeStamp, self.stats_info[9], None)
                            return None
                elif self.stats_info[1] == 'short': # refill previous short trade
                    if b_size >= 2:
                        if np.log(stock_bp/futures_sp) <= avg:
                            update_info(long, 'long', stock_midQ, futures_midQ)
                            update_line(stockData.timeStamp, self.stats_info[9], 'long')
                            return long
                        else:
                            update_line(stockData.timeStamp, self.stats_info[9], None)
                            return None
        else:
            update_line(stockData.timeStamp, self.stats_info[9], None)
            return None
        

###################################################################
            #########################################
        #################################################
class SingleStock_SingleStockFuturesArbitrageStrategy_lgbm(Strategy):
    
    def __init__(self, stratID, stratName, stratAuthor, ticker, day):
        super(SingleStock_SingleStockFuturesArbitrageStrategy_lgbm, self).__init__(stratID, stratName, stratAuthor) #call constructor of parent
        self.ticker = ticker #public field
        self.day = day #public field
        #timestamp, direction, stock_ticker, stock_price, stock_position, futrues_ticker, futures_price, futures_position, cash
        self.stats_info = [None, None, 'stock2330', None, 0, 'futureCDFD9', None, 0, 0, 0, 'lgbm+Spread'] 
        self.model = joblib.load('lgbm_class.sav') #lgbm.sav #lgbm_v2.pkl
        self.dataset = pd.DataFrame(columns=['stock_p_ask1', 'stock_p_bid1', 'stock_s_ask1', 'stock_s_bid1',
                                              'futures_p_ask1', 'futures_p_bid1', 'futures_s_ask1', 'futures_s_bid1',
                                              'stock_p_ask2', 'stock_p_bid2', 'stock_s_ask2', 'stock_s_bid2',
                                              'futures_p_ask2', 'futures_p_bid2', 'futures_s_ask2', 'futures_s_bid2',
                                              'stock_p_ask3', 'stock_p_bid3', 'stock_s_ask3', 'stock_s_bid3',
                                              'futures_p_ask3', 'futures_p_bid3', 'futures_s_ask3', 'futures_s_bid3',
                                              'stock_p_ask4', 'stock_p_bid4', 'stock_s_ask4', 'stock_s_bid4',
                                              'futures_p_ask4', 'futures_p_bid4', 'futures_s_ask4', 'futures_s_bid4',
                                              'stock_p_ask5', 'stock_p_bid5', 'stock_s_ask5', 'stock_s_bid5',
                                              'futures_p_ask5', 'futures_p_bid5', 'futures_s_ask5', 'futures_s_bid5',
                                              'stock_midQ', 'futures_midQ', 'spread'])
        # self.dataset = pd.DataFrame(columns=['stock_s_ask1', 'stock_s_bid1', 'futures_s_ask1', 'futures_s_bid1',
        #                                      'stock_s_ask2', 'stock_s_bid2','futures_s_ask2', 'futures_s_bid2',
        #                                      'stock_s_ask3', 'stock_s_bid3','futures_s_ask3', 'futures_s_bid3',
        #                                      'stock_s_ask4', 'stock_s_bid4', 'futures_s_ask4', 'futures_s_bid4',
        #                                      'stock_s_ask5', 'stock_s_bid5','futures_s_ask5', 'futures_s_bid5',
        #                                      'stock_midQ', 'futures_midQ', 'spread'])
        
       # self.figure, self.ax = plt.subplots(figsize=(8,6))
       # self.pnl, = self.ax.plot([], [])
        self.cash_list, self.timeStamp, self.direction = [], [], []

        #self.fig = plt.figure()
    def getStratDay(self):
        return self.day
    
    
    def run(self, stockData, futuresData, execution):
        #print(stockData.outputAsDataFrame(),isinstance(stockData, OrderBookSnapshot_FiveLevels))
        #print(futuresData.outputAsDataFrame(), isinstance(futuresData, OrderBookSnapshot_FiveLevels))
        #print(execution.outputAsArray())
        def update_info(info, trade, stock_midQ, futures_midQ):
            #self.stats_info[1] = last_trade
            self.stats_info[3] = info[2]
            self.stats_info[6] = info[6]
            # if trade == 'refill':
            #     self.stats_info[4] = 0
            #     self.stats_info[7] = 0
            flag = 0
            if trade == 'long':
                flag = -1
                self.stats_info[4] = self.stats_info[4] + 2
                self.stats_info[7] = self.stats_info[7] - 2
            elif trade == 'short':
                flag = 1
                self.stats_info[4] = self.stats_info[4] - 2
                self.stats_info[7] = self.stats_info[7] + 2
            #self.stats_info[9] = self.stats_info[8] + (self.stats_info[4] * stock_midQ + self.stats_info[7] * futures_midQ)
            self.stats_info[8] = self.stats_info[8] + flag*(2 * float(self.stats_info[3]) - 2 * float(self.stats_info[6]))
            self.stats_info[9] = self.stats_info[8] + (self.stats_info[4] * stock_midQ + self.stats_info[7] * futures_midQ)
            if self.stats_info[1] is None or self.stats_info[1] == 'refill':
                self.stats_info[1] = trade
            else:
                self.stats_info[1] = 'refill'
        
        def update_line(timeStamp, cash, direction):
            #self.pnl.set_xdata(np.append(self.pnl.get_xdata(), int(timeStamp.strftime("%H%M%S"))))
            #self.pnl.set_ydata(np.append(self.pnl.get_ydata(), cash))
            #plt.draw()
            #self.figure.canvas.draw()
            #plt.show()
            plt.close()
            if direction is not None:
                self.cash_list.append(cash)
                self.timeStamp.append(int(timeStamp.strftime("%H%M%S")))
                plt.plot(self.timeStamp, self.cash_list)
                plt.title('LGBM + MidQ Spread Arbitrage Strategy with 1 Std')
                plt.xlabel('Time')
                plt.ylabel('Total Asset')
                plt.show()
            else:
                if len(self.timeStamp) >= 1 and int(timeStamp.strftime("%H%M%S")) - self.timeStamp[-1]>3000:
                       self.cash_list.append(self.cash_list[-1])
                       self.timeStamp.append(int(timeStamp.strftime("%H%M%S")))
                       plt.plot(self.timeStamp, self.cash_list)
                       plt.title('LGBM + MidQ Spread Arbitrage Strategy with 1 Std')
                       plt.xlabel('Time')
                       plt.ylabel('Total Asset')
                       plt.show()
                       self.stats_info[0] = time.asctime(time.localtime(time.time()))
                       print('Current position and cash stats: >>>>>')
                       print(self.stats_info)
            

        if (stockData is None) and (futuresData is None) and (execution is None):
            return None
        
        elif (stockData is None) and (futuresData is None) and ((execution is not None) and (isinstance(execution, SingleStockExecution))): # Try to refill last trade
            #handle executions
            
            if execution.ticker == 'futureCDFD9':
                print('[%d] Strategy.handle_execution' % (os.getpid()))
                self.stats_info[0] = execution.timeStamp     
                print('Current position and cash stats: >>>>>')
                print(self.stats_info)
            
        
        elif (((stockData is not None) and (futuresData is not None)) and (isinstance(stockData, OrderBookSnapshot_FiveLevels))) and (execution is None): # Try to create a new trade
            #handle new market data, then create a new order and send it via quantTradingPlatform
            #print(futures_sp, futures_bp, stock_sp, s_size, stock_bp, b_size)
            multiple = 1
            #print(self.stats_info)
            ### If there is no order placed before
            stock = stockData.outputAsDataFrame()
            futures = futuresData.outputAsDataFrame()
            upper_thred = 0.99
            lower_thred = 0.1
            for i in range(1, 6):
                self.dataset['stock_p_ask'+str(i)] = stock['askPrice'+str(i)]
                self.dataset['stock_p_bid'+str(i)] = stock['bidPrice'+str(i)]
                self.dataset['stock_s_ask'+str(i)] = stock['askSize'+str(i)]
                self.dataset['stock_s_bid'+str(i)] = stock['bidSize'+str(i)]
                self.dataset['futures_p_ask'+str(i)] = futures['askPrice'+str(i)]
                self.dataset['futures_p_bid'+str(i)] = futures['bidPrice'+str(i)]
                self.dataset['futures_s_ask'+str(i)] = futures['askSize'+str(i)]
                self.dataset['futures_s_bid'+str(i)] = futures['bidSize'+str(i)]
            self.dataset['stock_midQ'] = (stock['askPrice'+str(i)]+stock['bidPrice'+str(i)])/2
            self.dataset['futures_midQ'] = (futures['askPrice'+str(i)]+futures['bidPrice'+str(i)])/2
            self.dataset['spread'] = np.log(self.dataset['stock_midQ']/self.dataset['futures_midQ'])
            
            
            #res = self.model.predict_proba(self.dataset)[:,1][0]
            #print(res)
            avg = -0.00014307391813982525
            std = 0.0016686150246379538
            futures_sp = futuresData.bidPrice1
            futures_bp = futuresData.askPrice1
            stock_sp, s_size = stockData.bidPrice1, stockData.bidSize1
            stock_bp, b_size = stockData.askPrice1, stockData.askSize1
            spread = np.log((stock_sp+stock_bp)/(futures_sp+futures_bp))
            long = ('stock2330', 'buy', stock_bp, 2, 'futureCDFD9', 'sell', futures_sp, 1)
            short = ('stock2330', 'sell', stock_sp, 2, 'futureCDFD9', 'buy', futures_bp, 1)
            futures_midQ = (futures_sp+futures_bp)/2
            stock_midQ = (stock_sp+stock_bp)/2
                #print(self.stats_info)
                ### If there is no order placed before
            if self.stats_info[1] is None or self.stats_info[1] == 'refill':
                if s_size < 2 and b_size < 2: # cannot place order
                    update_line(stockData.timeStamp, self.stats_info[9], None)
                    return None
                elif s_size < 2 and b_size >= 2: # can only long spread
                    #spread = np.log(stock_bp/futures_sp) #calculate long spread  
                    res = self.model.predict_proba(self.dataset)[:,1][0]
                    if spread < avg - multiple*std and res>upper_thred: # if not achieve the spread return None
                        # print(stockData.timeStamp)
                        update_info(long, 'long',stock_midQ, futures_midQ)
                        update_line(stockData.timeStamp, self.stats_info[9], 'long')
                        return long
                    else:
                        update_line(stockData.timeStamp, self.stats_info[9], None)
                        return None
                elif s_size >= 2 and b_size < 2: # can only short spread
                    #spread = np.log(stock_sp/futures_bp)
                    res = self.model.predict_proba(self.dataset)[:,1][0]
                    if spread > avg + multiple*std and res<lower_thred: # if not achieve the spread return None
                        # print(stockData.timeStamp)
                        update_info(short, 'short')
                        update_line(stockData.timeStamp, self.stats_info[9], 'short')
                        return short
                    else:
                        update_line(stockData.timeStamp, self.stats_info[9], None)
                        return None
                else: # both do either short or long
                    res = self.model.predict_proba(self.dataset)[:,1][0]
                    if spread < avg - multiple*std and res>upper_thred:
                        # print(stockData.timeStamp)
                        update_info(long, 'long',stock_midQ, futures_midQ)
                        update_line(stockData.timeStamp, self.stats_info[9], 'long')
                        return long
                    #elif diff_s >=0 and diff_s > diff_b and res<lower_thred:
                    elif spread > avg + multiple*std and res<lower_thred:
                        # print(stockData.timeStamp)
                        update_info(short, 'short',stock_midQ, futures_midQ)
                        update_line(stockData.timeStamp, self.stats_info[9], 'short')
                        return short
                    else:
                        update_line(stockData.timeStamp, self.stats_info[9], None)
                        return None
            else:
                if self.stats_info[1] == 'long':
                    if s_size >= 2:
                        if spread >= avg:# and res>upper_thred:
                        #if np.log(stock_sp/futures_bp) >= avg:
                            update_info(short, 'short',stock_midQ, futures_midQ)
                            update_line(stockData.timeStamp, self.stats_info[9], 'refill')
                            return short
                        else:
                            update_line(stockData.timeStamp, self.stats_info[9], None)
                            return None
                elif self.stats_info[1] == 'short': # refill previous short trade
                    if b_size >= 2:
                        if spread <= avg:# and res<lower_thred:
                        #if np.log(stock_bp/futures_sp) <= avg:
                            update_info(long, 'long',stock_midQ, futures_midQ)
                            update_line(stockData.timeStamp, self.stats_info[9], 'refill')
                            return long
                        else:
                            update_line(stockData.timeStamp, self.stats_info[9], None)
                            return None
        else:
            update_line(stockData.timeStamp, self.stats_info[9], None)
            return None
            

########################################
            
class SingleStockPosition():

    def __init__(self):

        self.timeStamp = None
        self.cash_long = 0
        self.cash_sell = 0
        self.position_long = 0 # long future short stock
        self.position_sell = 0 # short future long stock
        self.marketValue_long = 0
        self.marketValue_sell = 0
        self.netvalue_long = 0
        self.netvalue_sell = 0
        self.total_value = self.netvalue_long + self.netvalue_sell


    def outputAsArray(self):
        output = []
        output.append(self.date)
        output.append(self.ticker)
        output.append(self.timeStamp)
        output.append(self.cash)
        output.append(self.position)
        output.append(self.snapPrice)
        output.append(self.marketValue)

        return output
    
# class SingleStock_SingleStockFuturesArbitrageStrategy_rsi(Strategy):
#
#     def __init__(self, stratID, stratName, stratAuthor, ticker, day):
#         super(SingleStock_SingleStockFuturesArbitrageStrategy_rsi, self).__init__(stratID, stratName,
#                                                                               stratAuthor)  # call constructor of parent
#         self.ticker = ticker  # public field
#         self.day = day  # public field
#         # timestamp, direction, stock_ticker, stock_price, stock_position, futrues_ticker, futures_price, futures_position, cash
#         self.stats_info = [None, None, 'stock2330', None, None, 'futureCDFD9', None, None, 0, 0, 'RSI']
#         self.columns = ['timeStamp','futures_ap1','futures_bp1','futures_as1','futures_bs1','stock_ap1','stock_bp1','stock_as1','stock_bs1']
#         # remember historical data
#         self.memory_df = pd.DataFrame(columns = self.columns)
#         self.rsi_window = 14
#         self.position = SingleStockPosition()
#         self.position_prvious = SingleStockPosition()
#         self.size = 2000
#         self.stats_info = [None, None, 'stock2330', None, 0, 'futureCDFD9', None, 0, 0, 'RSI']
#         self.cash_list, self.timeStamp = [], []
#
#     def getStratDay(self):
#         return self.day
#
#     def run(self, stockData, futuresData,execution):
#
#         def update_info(info, trade, stock_midQ, futures_midQ):
#             #self.stats_info[1] = last_trade
#             self.stats_info[3] = info[2]
#             self.stats_info[6] = info[6]
#             # if trade == 'refill':
#             #     self.stats_info[4] = 0
#             #     self.stats_info[7] = 0
#             flag = 0
#             if trade == 'long':
#                 flag = -1
#                 self.stats_info[4] = self.stats_info[4] + 2
#                 self.stats_info[7] = self.stats_info[7] - 2
#             elif trade == 'short':
#                 flag = 1
#                 self.stats_info[4] = self.stats_info[4] - 2
#                 self.stats_info[7] = self.stats_info[7] + 2
#             self.stats_info[8] = self.stats_info[8] + flag*(2 * float(self.stats_info[3]) - 2 * float(self.stats_info[6]))
#             self.stats_info[9] = self.stats_info[8] + (self.stats_info[4] * stock_midQ + self.stats_info[7] * futures_midQ)
#             if self.stats_info[1] is None or self.stats_info[1] == 'refill':
#                 self.stats_info[1] = trade
#             else:
#                 self.stats_info[1] = 'refill'
#
#         def update_line(timeStamp, cash, direction):
#             #self.pnl.set_xdata(np.append(self.pnl.get_xdata(), int(timeStamp.strftime("%H%M%S"))))
#             #self.pnl.set_ydata(np.append(self.pnl.get_ydata(), cash))
#             #plt.draw()
#             #self.figure.canvas.draw()
#             #plt.show()
#             plt.close()
#             if direction is not None:
#                 self.cash_list.append(cash)
#                 self.timeStamp.append(int(timeStamp.strftime("%H%M%S")))
#                 plt.plot(self.timeStamp, self.cash_list)
#                 plt.title('RSI Indicator Arbitrage Strategy')
#                 plt.xlabel('Time')
#                 plt.ylabel('Total Asset')
#                 plt.show()
#             else:
#                 if len(self.timeStamp) >= 1 and int(timeStamp.strftime("%H%M%S")) - self.timeStamp[-1]>3000:
#                        self.cash_list.append(self.cash)
#                        self.timeStamp.append(int(timeStamp.strftime("%H%M%S")))
#                        plt.plot(self.timeStamp, self.cash_list)
#                        plt.title('RSI Indicator Arbitrage Strategy')
#                        plt.xlabel('Time')
#                        plt.ylabel('Total Asset')
#                        plt.show()
#                        self.stats_info[0] = time.asctime(time.localtime(time.time()))
#                        print(self.stats_info)
#
#
#         if (stockData is None) and (futuresData is None) and (execution is None):
#             return None
#
#         elif (stockData is None) and (futuresData is None) and ((execution is not None) and (isinstance(execution, SingleStockExecution))):
#             # handle executions
#             if execution.ticker == 'futureCDFD9':
#                 print('[%d] Strategy.handle_execution' % (os.getpid()))
#                 self.stats_info[0] = execution.timeStamp
#                 print('Current position and cash stats: >>>>>')
#                 print(self.stats_info)
#
#
#             return None
#         elif (((stockData is not None) and (futuresData is not None)) and (isinstance(stockData, OrderBookSnapshot_FiveLevels))) \
#                 and (execution is None):  # Try to create a new trade
#             # handle new market data, then create a new order and send it via quantTradingPlatform.
#             futures_bp,futures_bs = futuresData.bidPrice1,futuresData.bidSize1
#             futures_ap,futures_as= futuresData.askPrice1,futuresData.askSize1
#             stock_bp, stock_bs = stockData.bidPrice1, stockData.bidSize1
#             stock_ap, stock_as = stockData.askPrice1, stockData.askSize1
#             futures_midQ = (futures_ap+futures_bp)/2
#             stock_midQ = (stock_ap+stock_bp)/2
#             long = ('stock2330', 'buy', stock_bp, 2, 'futureCDFD9', 'sell', futures_ap, 1)
#             short = ('stock2330', 'sell', stock_ap, 2, 'futureCDFD9', 'buy', futures_bp, 1)
#             df_temp = pd.DataFrame(data = [[stockData.timeStamp,futures_ap,futures_bp,futures_as,futures_bs,stock_ap,stock_bp,stock_as,stock_bs]],
#                                    columns = self.columns)
#             self.memory_df = self.memory_df.append(df_temp)
#             if self.memory_df.shape[0]< self.rsi_window:
#                 return None
#             else:
#                 # short the future and buy the stock
#                 spread1 = self.memory_df['futures_ap1'] - self.memory_df['stock_bp1']
#                 # long the future and short the stock
#                 spread2 = self.memory_df['futures_bp1'] - self.memory_df['stock_ap1']
#                 rsi1 = ta.RSI(spread1,self.rsi_window)
#                 rsi2 = ta.RSI(spread2, self.rsi_window)
#
#                 if (stock_as < 2)&(stock_bs < 2):
#                     return None
#                 elif (stock_as > 2)&(stock_bs< 2): # short the stock  and long future only
#                     pass
#                 elif (stock_as < 2)&(stock_bs> 2): # long the stock and short future only
#                     pass
#                 elif (stock_as> 2) & (stock_bs> 2): # both are permitted
#
#                     if (rsi1.iloc[-2]<70) & (rsi1.iloc[-1]>70):
#                         self.position.position_sell = -1 # sell the future and long the stock
#                         update_info(long, 'long', stock_midQ, futures_midQ)
#                         update_line(stockData.timeStamp, self.stats_info[9], 'long')
#                         return long
#                     elif (rsi1.iloc[-2]>30) & (rsi1.iloc[-1]<30):
#                         self.position.position_sell = 0
#                         update_info(short, 'short', stock_midQ, futures_midQ)
#                         update_line(stockData.timeStamp, self.stats_info[9], 'short')
#                         return short
#
#                     if self.position_prvious.position_sell == self.position.position_sell:
#                         self.position.cash_sell = self.position.cash_sell
#                     if (self.position_prvious.position_sell==0) & (self.position.position_sell == -1):
#                         self.position.cash_sell = self.position.cash_sell + self.size*(futures_bp - stock_ap )
#                     if (self.position_prvious.position_sell == -1) & (self.position.position_sell == 0):
#                         self.position.cash_sell = self.position.cash_sell + self.size * ( - futures_ap + stock_bp)
#                     self.position.marketValue_sell = self.position.position_sell * (futures_ap - stock_bp) * self.size
#                     self.position.netvalue_sell = self.position.cash_sell + self.position.marketValue_sell
#
#                     if (rsi2.iloc[-2]>30) & (rsi2.iloc[-1]<30):
#                         self.position.position_buy = 1 # long the future and short stock
#                         update_info(short, 'short', stock_midQ, futures_midQ)
#                         update_line(stockData.timeStamp, self.stats_info[9], 'short')
#                         return short
#                     elif (rsi2.iloc[-2]<70) & (rsi2.iloc[-2]>70):
#                         self.position.position_buy = 0
#                         update_info(long, 'long', stock_midQ, futures_midQ)
#                         update_line(stockData.timeStamp, self.stats_info[9], 'long')
#                         return long
#
#                     if self.position_prvious.position_long == self.position.position_long:
#                         self.position.cash_long = self.position.cash_long
#                     if (self.position_prvious.position_long==0) & (self.position.position_long == 1):
#                         self.position.cash_long = self.position.cash_long - self.size*(futures_ap - stock_sp )
#                     if (self.position_prvious.position_long == 1) & (self.position.position_long == 0):
#                         self.position.cash_long = self.position.cash_long - self.size * ( - futures_bp + stock_ap)
#                     self.position.marketValue_long = self.position.position_long * (futures_bp - stock_ap) * self.size
#                     self.position.netvalue_long = self.position.cash_long + self.position.marketValue_long
#                     self.position.total_value = self.position.netvalue_sell + self.position.netvalue_long
#
#                     #print('The pnl at the this time '+str(stockData.timeStamp)+' is '+ str(self.position.total_value))
#                     self.position_prvious = self.position
#
#         else:
#             return None
