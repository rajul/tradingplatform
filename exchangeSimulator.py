# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 10:12:21 2020

@author: hongsong chou
"""

import threading
import os
import time
from common.SingleStockExecution import SingleStockExecution


class BuyOrderQueue:
    def __init__(self):
        self.q = []

    def put(self, order):
        self.q.append(order)

    def get_order(self):
        self.q = sorted(self.q, key=lambda x: x.price, reverse=True)
        res = None
        if self.q:
            res = self.q[0]
            self.q.remove(res)

        return res


class SellOrderQueue:
    def __init__(self):
        self.q = []

    def put(self, order):
        self.q.append(order)

    def get_order(self):
        self.q = sorted(self.q, key=lambda x: x.price)
        res = None
        if self.q:
            res = self.q[0]
            self.q.remove(res)

        return res


class ExchangeSimulator:
    def __init__(self, marketData_2_exchSim_q, platform_2_exchSim_order_q, exchSim_2_platform_execution_q):
        print("[%d]<<<<< call ExchSim.init" % (os.getpid(),))

        self.orderbook = {}
        self.BuyOrderQueue = BuyOrderQueue()
        self.SellOrderQueue = SellOrderQueue()

        self._Lock = threading.Lock()

        t_md = threading.Thread(name='exchsim.on_md', target=self.consume_md, args=(marketData_2_exchSim_q,))
        t_md.start()

        t_order = threading.Thread(name='exchsim.on_order', target=self.consume_order,
                                   args=(platform_2_exchSim_order_q, exchSim_2_platform_execution_q,))
        t_order.start()

    def consume_md(self, marketData_2_exchSim_q):
        while True:
            # self._Lock.acquire()
            res = marketData_2_exchSim_q.get()
            #print('[%d]ExchSim.consume_md' % (os.getpid()))
            self.orderbook[res.outputAsDataFrame().loc[0]['ticker']] = res.outputAsDataFrame().loc[0]
            # self._Lock.release()

    def consume_order(self, platform_2_exchSim_order_q, exchSim_2_platform_execution_q):
        while True:
            res = platform_2_exchSim_order_q.get()

            print("Getting an order")
            print(res.outputAsArray())

            if res.direction == 'buy':
                if res.type == 'MO':
                    res.price = float('inf')
                print('push into buy order queue')
                self.BuyOrderQueue.put(res)
            else:
                if res.type == 'MO':
                    res.price = -float('inf')
                print('push into sell order queue')
                self.SellOrderQueue.put(res)

            print("Order in queue")
            res_buy = self.BuyOrderQueue.get_order()
            res_sell = self.SellOrderQueue.get_order()
            # print(res_buy)
            # print(res_sell)

            if res_buy is not None:
                # print(res_buy.outputAsArray())
                self.produce_execution(res_buy, exchSim_2_platform_execution_q)
            if res_sell is not None:
                # print(res_sell.outputAsArray())
                self.produce_execution(res_sell, exchSim_2_platform_execution_q)

    def produce_execution(self, order, exchSim_2_platform_execution_q):
        self._Lock.acquire()
        execution = SingleStockExecution(order.ticker, order.date, time.asctime(time.localtime(time.time())))
        execution.direction = order.direction
        if order.type == 'MO':
            if order.direction == 'buy':
                for i in range(1, 6):
                    execution.price = self.orderbook[order.ticker]['askPrice{}'.format(i)]
                    execution.size = min(self.orderbook[order.ticker]['askSize{}'.format(i)], order.size)
                    exchSim_2_platform_execution_q.put(execution)
                    order.size = order.size - execution.size
                    if order.size > 0:
                        order.type = 'LO'
                        order.price = execution.price
                        self.BuyOrderQueue.put(order)
                    else:
                        break
            elif order.direction == 'sell':
                for i in range(1, 6):
                    execution.price = self.orderbook[order.ticker]['bidPrice{}'.format(i)]
                    execution.size = min(self.orderbook[order.ticker]['bidSize{}'.format(i)], order.size)
                    exchSim_2_platform_execution_q.put(execution)
                    order.size = order.size - execution.size
                    if order.size > 0:
                        order.type = 'LO'
                        order.price = execution.price
                        self.SellOrderQueue.put(order)
                    else:
                        break
        elif order.type == 'LO':
            if order.direction == 'buy':
                for i in range(1, 6):
                    if order.size == 0 or order.price < self.orderbook[order.ticker]['askPrice{}'.format(i)]:
                        break
                    execution.price = self.orderbook[order.ticker]['askPrice{}'.format(i)]
                    execution.size = min(self.orderbook[order.ticker]['askSize{}'.format(i)], order.size)
                    exchSim_2_platform_execution_q.put(execution)
                    order.size = order.size - execution.size
                if order.size > 0:
                    self.BuyOrderQueue.put(order)
            elif order.direction == 'sell':
                for i in range(1, 6):
                    if order.size == 0 or order.price > self.orderbook[order.ticker]['bidPrice{}'.format(i)]:
                        break
                    execution.price = self.orderbook[order.ticker]['bidPrice{}'.format(i)]
                    execution.size = min(self.orderbook[order.ticker]['bidSize{}'.format(i)], order.size)
                    exchSim_2_platform_execution_q.put(execution)
                    order.size = order.size - execution.size
                if order.size > 0:
                    self.SellOrderQueue.put(order)

        self._Lock.release()
